#include "cs510.h"
#include "request.h"
#include "pthread.h"

// 
// server.c: A very, very simple web server
//
// To run:
//  server <portnum (above 2000)>
//
// Repeatedly handles HTTP requests sent to this port number.
// Most of the work is done within routines written in request.c
//

// Honglue: create a new strcture called buffer
typedef struct buffer
{
	int nBuffer; // total size of buffer
	int nEmpty; // number of empty buffer
	int nOccupy; // number of occupied buffer
	int input_idx; // index using for input_buffer
	int output_idx; // index using for output_buffer

	int* Buffer; // buffer array pointer
	
	pthread_mutex_t mutex; // buffer mutex
	pthread_cond_t hasEmpty; // buffer condition1
	pthread_cond_t hasOccupy; // buffer condition2
	
} buffer_t;

// CS510: Parse the new arguments too

void getargs(int *a, int *b, int *c, int argc, char *argv[])
{
    // Honglue: add argument: worker is worker thread and buffer is buffer 
    if (argc != 4) {
	fprintf(stderr, "Usage: %s <port> <worker> <buffer>\n", argv[0]);
	exit(1);
    }
    *a = atoi(argv[1]);
    *b = atoi(argv[2]);
    *c = atoi(argv[3]);
}

// Honglue: initialize the buffer structure
void init_buffer(buffer_t *b, int nBuffer) {
	
	pthread_mutex_init(&b->mutex, NULL);
	pthread_cond_init(&b->hasEmpty, NULL);
	pthread_cond_init(&b->hasOccupy, NULL);
	
    // Honglue: assign all the values in the buffer structure
	pthread_mutex_lock(&b->mutex);
	
	b->Buffer = malloc(nBuffer * sizeof(int));
	b->nBuffer = nBuffer;
	b->nEmpty = nBuffer;
	b->nOccupy = 0;
	b->input_idx = 0;
	b->output_idx = 0;
	
	pthread_mutex_unlock(&b->mutex);
	
}

// Honglue: input a connfd into the buffer array
void input_buffer(buffer_t *b, int input_connfd) {
	
	pthread_mutex_lock(&b->mutex);
	
	while (b->nEmpty == 0) {
		pthread_cond_wait(&b->hasEmpty, &b->mutex);
	}
	
	b->Buffer[b->input_idx] = input_connfd;
	b->nEmpty--;
	b->nOccupy++;
	b->input_idx = (b->input_idx + 1) % b->nBuffer;
	pthread_cond_signal(&b->hasOccupy);
	
	pthread_mutex_unlock(&b->mutex);
}

// Honglue: output a connfd from the buffer array
int output_buffer(buffer_t *b) {
	
	int output_connfd = -1;
	
	pthread_mutex_lock(&b->mutex);
	
	while (b->nOccupy == 0) {
		pthread_cond_wait(&b->hasOccupy, &b->mutex);
	}
	
	output_connfd = b->Buffer[b->output_idx];
	
	b->nEmpty++;
	b->nOccupy--;
	b->output_idx = (b->output_idx + 1) % b->nBuffer;
	pthread_cond_signal(&b->hasEmpty);
	
	pthread_mutex_unlock(&b->mutex);
	return output_connfd;
}


void *handle(buffer_t *b)
{
    printf("Starting a thread ...\n");
	while(1) {
		int current_connfd = output_buffer(b);
		printf("CheckPoint: getting connfd: %d. Now Sleeping ...\n",current_connfd);
		sleep(5);
	
		requestHandle(current_connfd);
		printf("CheckPoint: request Handle: %d. Now Sleeping ...\n",current_connfd);
		sleep(5);
	
		Close(current_connfd);
		printf("CheckPoint: close connfd: %d.\n",current_connfd);
	}
}

int main(int argc, char *argv[])
{
    int listenfd, connfd, clientlen;
    int i;
    int port, nThread, nBuffer;
    struct sockaddr_in clientaddr;
    
    printf("port: %p nThread: %p nBuffer: %p\n",(void*)&port,(void*)&nThread,(void*)&nBuffer);

    getargs(&port, &nThread, &nBuffer, argc, argv);


    printf("port: %d nThread: %d nBuffer: %d\n",port,nThread,nBuffer);
    
	buffer_t *b;
	init_buffer(b,nBuffer); // Honglue: initialize a buffer

    // Honglue: check if the buffer structure is successfully initialized
    printf("%d %d %d %d %d\n",b->nBuffer,b->nEmpty,b->nOccupy,b->input_idx,b->output_idx);
	
    // 
    // CS510: Create some threads...
    //

	pthread_t thread_id[nThread];
	
	for (i=0; i<nThread; i++) {
		pthread_create(&thread_id[i],NULL,handle,b);
	}
	
    listenfd = Open_listenfd(port);
    
    //Starting the loop
    while (1) {
		clientlen = sizeof(clientaddr);
		printf("CheckPoint: waiting for workers.\n");
	
		connfd = Accept(listenfd, (SA *)&clientaddr, (socklen_t *) &clientlen);
		printf("CheckPoint: received connfd: %d.\n",connfd);
	
	// 
	// CS510: In general, don't handle the request in the main thread.
	// Save the relevant info in a buffer and have one of the worker threads 
	// do the work. However, for SFF, you may have to do a little work
	// here (e.g., a stat() on the filename) ...
	// 
        
        // input the connfd into the buffer array    
	    input_buffer(b,connfd);
	
    }
    
    
    return 0;

}

