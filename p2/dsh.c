#include "dsh.h"

void seize_tty(pid_t callingprocess_pgid); /* Grab control of the terminal for the calling process pgid.  */
void continue_job(job_t *j); /* resume a stopped job */
void spawn_job(job_t *j, bool fg); /* spawn a new job */
job_t* loc_job(pid_t pid); /* Locate a job given a pgid */

int logfd, termfd; /* logfd: logfile descriptor, termfd: terminal file descriptor*/
int pid, oldin, oldout;
int status;
pid_t dsh_pid;
char* prompt;
job_t* first_job;
job_t* cur_job;

/* Write error message to log file */
void unix_error(char *error)
{
    perror(error); /* print error */
    char *msg = malloc(sizeof(error)); /* assign a massage memory */
    sprintf(msg, "%s\n", error); /* write error to message msg */
    write(logfd, msg, strlen(msg)); /* write message into file */
    //printf("Write to log file\n"); /////////////////////////////////////////////
    free(msg); /* free memory */
}

/* Sets the process group id for a given job and process */
int set_child_pgid(job_t *j, process_t *p)
{
    if (j->pgid < 0) /* first child: use its pid for job pgid */
        j->pgid = p->pid;
    return(setpgid(p->pid,j->pgid));
}

/* Creates the context for a new child by setting the pid, pgid and tcsetpgrp */
void new_child(job_t *j, process_t *p, bool fg)
{
        /* establish a new process group, and put the child in
         * foreground if requested
         */

        /* Put the process into the process group and give the process
         * group the terminal, if appropriate.  This has to be done both by
         * the dsh and in the individual child processes because of
         * potential race conditions.  
         * */

        p->pid = getpid();

        /* also establish child process group in child to avoid race (if parent has not done it yet). */
        set_child_pgid(j, p);

        if(fg) // if fg is set
        seize_tty(j->pgid); // assign the terminal

        /* Set the handling for job control signals back to the default. */
        signal(SIGTTOU, SIG_DFL);
}

/* Spawning a process with job control. fg is true if the 
 * newly-created process is to be placed in the foreground. 
 * (This implicitly puts the calling process in the background, 
 * so watch out for tty I/O after doing this.) pgid is -1 to 
 * create a new job, in which case the returned pid is also the 
 * pgid of the new job.  Else pgid specifies an existing job's 
 * pgid: this feature is used to start the second or 
 * subsequent processes in a pipeline.
 * */

void spawn_job(job_t *j, bool fg) 
{

	pid_t pid;
	process_t *p;
	
	int last_pipeline[2]; /* record old pipeline fd */
	
	//fprintf(stderr, "%d(Launched): %s\n", j->pgid, j->commandinfo); /////////////////////////////////////////////
	
	for(p = j->first_process; p; p = p->next) {
		
		char launch_msg[100]; /////////////////////////////////////////////
        sprintf(launch_msg, "%d(Launched): %s\n", j->pgid, j->commandinfo); // wrong!!! /////////////////////////////////////////////
        write(logfd, launch_msg, strlen(launch_msg)); /////////////////////////////////////////////
		
		int cur_pipeline[2]; /* record new pipeline fd */
		
		
	/* YOUR CODE HERE? */
	/* Builtin commands are already taken care earlier */
	    if (p->next != NULL) {
	    	pipe(cur_pipeline);
		}
	    
	    switch (pid = fork()) {

            case -1: /* fork failure */
            perror("fork");
            unix_error("Error: Fork error!");
            exit(EXIT_FAILURE);

            case 0: /* child process */
            p->pid = getpid();	    
            new_child(j, p, fg);
			
			if (p->next != NULL){
				close(cur_pipeline[0]);
				dup2(cur_pipeline[1],1);
				close(cur_pipeline[1]); 
			}

			if (p != j->first_process){
				close(last_pipeline[1]);
					dup2(last_pipeline[0],0);			
					close(last_pipeline[0]);	
			}
			
			/////////////////////////////////////////////
			//handle process input or output file other than stdin, stdout
    		if(p->ifile != NULL) {
      			oldin = dup(STDIN_FILENO);
      			int fd = open(p->ifile, O_RDONLY);
      			dup2(fd, STDIN_FILENO);
      			close(fd);
      			dup2(oldin,fd);
    		}
    		
    		/////////////////////////////////////////////
    		if(p->ofile != NULL) {
      			oldout = dup(STDOUT_FILENO);
      			int fd = open(p->ofile, O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR | S_IROTH);
      			dup2(fd, STDOUT_FILENO);
      			close(fd);
            	dup2(oldout,fd);
    		}
			
			/////////////////////////////////////////////
			if(p->argv[0] != NULL) {
                p->stopped=false;
                if (execvp(p->argv[0], p->argv)<0) {
                    p->stopped=true;
				    p->completed=true;
                }
            }
            int s_fd = dup(STDERR_FILENO);
            int n_fd = dup2(logfd, STDERR_FILENO);
            close(logfd);
			
			/////////////////////////////////////////////
	        /* YOUR CODE HERE?  Child-side code for new process. */
            perror("New child should have done an exec");
            dup2(s_fd, n_fd);
            exit(EXIT_FAILURE);  /* NOT REACHED */
            break;    /* NOT REACHED */

            default: /* parent */
            /* establish child process group */
            p->pid = pid;
            set_child_pgid(j, p);
						
			if (p != j->first_process){		
				close(last_pipeline[1]);			
				close(last_pipeline[0]); 			
			}

			if (p->next != NULL){		
				last_pipeline[0] = cur_pipeline[0];
				last_pipeline[1] = cur_pipeline[1];
			}
			
            /* YOUR CODE HERE?  Parent-side code for new process.  */
            
    	}
    	
    	/////////////////////////////////////////////
        int status=0;
	    if(fg){
	        /* Wait for the job to complete */
				if (p->pid != 0) {
					waitpid(j->pgid, &status, WUNTRACED);

				}
				if (status == 0) {
				p->completed = true;
				p->stopped = true;
				}
				else {
				p->completed = false;
				p->stopped = true;
				}	
	    }

    /* YOUR CODE HERE?  Parent-side code for new job.*/
	if (p->ifile != NULL) {
        //close(input1);
        dup2(oldin, STDIN_FILENO);
    }
            
    if (p->ofile != NULL) {
        //close(output1);
        dup2(oldout, STDOUT_FILENO);
    }
    
//    dup2(termfd,STDIN_FILENO); //STDIN back to terminal fd
	seize_tty(getpid()); // assign the terminal back to dsh

	}
}

/* Sends SIGCONT signal to wake up the blocked job */
void continue_job(job_t *j) 
{
     if(kill(-j->pgid, SIGCONT) < 0)
          perror("kill(SIGCONT)");
          unix_error("Error: kill(SIGCONT)");
}

/* Locate a job given a pgid */
job_t* loc_job(pid_t pid) {
	job_t *job = first_job;
	while (job != NULL) {
		if (job->pgid != pid) job = job->next;
		else {
			return job;
		}
	}
	return NULL;
}

/* 
 * builtin_cmd - If the user has typed a built-in command then execute
 * it immediately.  
 */
bool builtin_cmd(job_t *last_job, int argc, char **argv) 
{
    /* check whether the cmd is a built in command
    */

    if (!strcmp(argv[0], "quit")) {
        /* Your code here */
        close(logfd);
        exit(EXIT_SUCCESS);
	}
    else if (!strcmp("jobs", argv[0])) {
        /* Your code here */
        job_t *j = last_job;
        while(j != NULL) {
        	char* job_status = malloc(100);
        	/* judge the status of current jobs */
            if (job_is_completed(j)) {
                sprintf(job_status, "%s", "Completed");
            }
            else {
            	if (job_is_stopped(j)) {
            		sprintf(job_status, "%s", "Stopped");
            	}
            	else {
            		sprintf(job_status, "%s", "In processing");
            	}
            }
            printf("jobs: %s pgid: %d status: %d %s\n",j->commandinfo,j->pgid,j->first_process->status,job_status);
            free(job_status);
            j = j->next;
        }
        last_job->first_process->status = 0;
        last_job->first_process->completed = true;
        return true;
    }
	else if (!strcmp("cd", argv[0])) {
        /* Your code here */
        if (argv[1] != NULL) {
        	/* if directory exists or not */
        	if(chdir(argv[1]) == -1) {
        		printf("%s\n", argv[1]);
        		unix_error("Error: No such directory!");
        	}
        	else {
        		last_job->first_process->status = 0;
        		last_job->first_process->completed = true;
        	}
        }
        return true;
    }
    else if (!strcmp("bg", argv[0])) {
        job_t *job;
		pid_t pid_key;
		/* if give a job pgid number, then use this pgid to locate a job */
        if (argv[1] != NULL) {
			pid_key = (pid_t) atoi(argv[1]);
			job = loc_job(pid_key);
		}
		/* else use the last job to foreground, not sure if this is right? */
		else {
			job = last_job;
		}
		if (job != NULL) {
			continue_job(job); /* do we just continue the job? */
			job->bg = true; /* job->bg to be set true */
		}
		else {
			unix_error("Error: No job to background!");
		}
        
        last_job->first_process->status = 0;
        last_job->first_process->completed = true;
        return true;
        /* Your code here */
    }
    else if (!strcmp("fg", argv[0])) {
        /* Your code here */
        
        job_t *job;
		pid_t pid_key;
		/* if give a job pgid number, then use this pgid to locate a job */
        if (argv[1] != NULL) {
			pid_key = (pid_t) atoi(argv[1]);
			job = loc_job(pid_key);
		}
		/* else use the last job to foreground, not sure if this is right? */
		else {
			job = last_job;
		}
		if (job != NULL) {
			continue_job(job); /* do we just continue the job? */
			job->bg = false; /* job->bg to be set false */
			/* handle foreground job */
			status = 0;
			waitpid(job->first_process->pid, &status, 0);
			job->first_process->status = 0;
			seize_tty(getpid());
		}
		else {
			unix_error("Error: No job to foreground!");
		}
        
        last_job->first_process->status = 0;
        last_job->first_process->completed = true;
        return true;
    }

    return false;       /* not a builtin command */
}

/* Build prompt messaage */
char* promptmsg() 
{
	/* Modify this to include pid */
    pid = getpid();
    prompt = (char*)malloc(100);
    sprintf(prompt,"dsh-%d$ ",pid);
    return prompt;
}

int main() 
{
	init_dsh();
	DEBUG("Successfully initialized\n");
	
	dsh_pid = getpid();
	logfd = open("dsh.log", O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR | S_IROTH); /* write log file */


	printf("# starting devil shell from a terminal...\n");
	printf("# devil shell started; waiting for inputs at the prompt.\n");
	printf("# Observe that the PID of dsh - %d is part of the prompt\n",dsh_pid);
	printf("# creating log file: dsh.log logfd: %d\n",logfd);
	
	termfd = STDIN_FILENO;
	
	while(1) {
		job_t *j = NULL;
		if(!(j = readcmdline(promptmsg()))) {
			if (feof(stdin)) { /* End of file (ctrl-d) */
				fflush(stdout);
				printf("\n");
				exit(EXIT_SUCCESS);
           	}
           	continue; /* NOOP; user entered return or spaces with return */
		}
		
		/* Kangnan */
		first_job = j; /* a pointer to always record the first job */
		cur_job = first_job; /* assign current job */
		
		while(cur_job != NULL) {
			/* Kangnan */
			cur_job->pgid = getpid(); /* assign process id to current job */
			bool isBuiltin = builtin_cmd(cur_job, cur_job->first_process->argc, cur_job->first_process->argv);
			if(!isBuiltin) {
				/* if current job is not background */
				if(!cur_job->bg) {
         			spawn_job(cur_job, !cur_job->bg);
         			/* handle foreground job */
         			status = 0;
         			waitpid(cur_job->first_process->pid, &status, 0);
         			cur_job->first_process->status = 0;
         		}
         		else {
         			spawn_job(cur_job, !cur_job->bg);
         		}
       	}

    	cur_job = cur_job->next;
    }

    /* Only for debugging purposes to show parser output; turn off in the
     * final code */
    //if(PRINT_INFO) print_job(j);
    //printf("==========================================================\n");

    /* Your code goes here */
    /* You need to loop through jobs list since a command line can contain ;*/
    /* Check for built-in commands */
    /* If not built-in */
        /* If job j runs in foreground */
        /* spawn_job(j,true) */
        /* else */
        /* spawn_job(j,false) */
	
	
    }
    
    close(logfd);
}
